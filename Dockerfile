#provisionamento de imagem linux
FROM ubuntu:19.10 

RUN apt-get update -y

RUN apt-get install ansible -y 
RUN ansible --version

#copiando script para dentro do container
COPY inventory .
COPY main.yml .

#executando o script para build da imagem
RUN ansible-playbook -i inventory main.yml